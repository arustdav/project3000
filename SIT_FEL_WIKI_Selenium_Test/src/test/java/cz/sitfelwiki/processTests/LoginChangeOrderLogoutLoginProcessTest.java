package cz.sitfelwiki.processTests;

import cz.sitfelwiki.WebDriverSettings;
import org.junit.Test;
import org.openqa.selenium.By;

public class LoginChangeOrderLogoutLoginProcessTest extends WebDriverSettings {

    @Test
    public void Login_ChangeOrderToDESC_Logout_LoginAgain() {
        login();
        driver.findElement(By.name("desc")).click(); // change the order of articles
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        logout();

        /*We want to check if our change saves after logout */
        login();
    }

}
