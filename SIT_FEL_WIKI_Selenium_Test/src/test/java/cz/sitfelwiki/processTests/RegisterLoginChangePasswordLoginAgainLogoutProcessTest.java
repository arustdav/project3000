package cz.sitfelwiki.processTests;

import cz.sitfelwiki.WebDriverSettings;
import org.junit.Test;
import org.openqa.selenium.By;

public class RegisterLoginChangePasswordLoginAgainLogoutProcessTest extends WebDriverSettings {

    String editedPassword = "editedpassword"; //new password

    @Test
    public void RegisterLoginChangePasswordTryToLoginAgainWithEditedPasswordLogout() {
        registerUserWithRandomCredentials();
        authorizeUserWithRandomCredentials();
        waitTillMainPageFullyLoaded();

        driver.findElement(By.className("dropdown")).click(); //opening dropdown menu
        driver.findElement(By.xpath("//a[contains(text(), 'Reset Your Password')]")).click();
        driver.findElement(By.name("new_password")).sendKeys(editedPassword); //input data
        driver.findElement(By.name("confirm_password")).sendKeys(editedPassword);
        driver.findElement(By.cssSelector("input[type='submit']")).click();
        driver.findElement(By.name("username")).sendKeys(shortUsername);
        driver.findElement(By.name("password")).sendKeys(editedPassword); //input new password
        driver.findElement(By.cssSelector("input[type='submit']")).click();
        logout();
    }

}
