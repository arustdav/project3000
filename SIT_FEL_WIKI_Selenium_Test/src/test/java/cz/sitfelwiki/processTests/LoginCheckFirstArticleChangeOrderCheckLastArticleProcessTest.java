package cz.sitfelwiki.processTests;

import cz.sitfelwiki.WebDriverSettings;
import org.junit.ComparisonFailure;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;

public class LoginCheckFirstArticleChangeOrderCheckLastArticleProcessTest  extends WebDriverSettings{

    @Test
    public void LoginChangeOrderGetOnTheLastPage_FirstArticleAfterLoginBecomesLastArticleAfterChangingOrder_FirstArticleGoesToTheLastPlace() {

         //ARRANGE & ACT
         login();
         String firstInTheTable = driver.findElement(By.xpath("//tr[1]/td[1]")).getText(); //Find the first article
         driver.findElement(By.xpath("//a[contains(text(), 'Last')]")).click(); // go to the last page

         String lastInTheTable = driver.findElement(By.xpath("//tr[last()]/td[1]")).getText(); // find the last article
         driver.findElement(By.xpath("//a[contains(text(), 'First')]")).click(); // go to the first page

         driver.findElement(By.name("desc")).click(); //change order

         driver.findElement(By.xpath("//a[contains(text(), 'Last')]")).click(); // go to the last page
         String lastAfterChangingOrder = driver.findElement(By.xpath("//tr[last()]/td[1]")).getText(); //find the last article


        /* After changing the order the first article must become the last.
           We want to be sure that it works correctly.
        */

        //ASSERT
        try{
            assertEquals(firstInTheTable, lastAfterChangingOrder);
        } catch (ComparisonFailure TheyAreNotEqual) {
            System.out.println("The function doesn't work properly!");
            assertEquals(lastInTheTable, lastAfterChangingOrder);
        }
    }

}
