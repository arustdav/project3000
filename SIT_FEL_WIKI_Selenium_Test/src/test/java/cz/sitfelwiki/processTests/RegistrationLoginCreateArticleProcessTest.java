package cz.sitfelwiki.processTests;

import cz.sitfelwiki.WebDriverSettings;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class RegistrationLoginCreateArticleProcessTest extends WebDriverSettings {


/**
    In this method we are trying to test if registration then login then create article are working normally.
    We want to be sure that user can use the main functions of application in order that is planned by application's architecture
*/

    @Test
    public void RegisterLoginCreateArticle() {

        registerUserWithRandomCredentials();
        //wait until login form is fully loaded
        wait.until(ExpectedConditions.presenceOfElementLocated(By.className("raidShadowLegends")));

        authorizeUserWithRandomCredentials();
        waitTillMainPageFullyLoaded();
        createRandomArticle();
    }





}
