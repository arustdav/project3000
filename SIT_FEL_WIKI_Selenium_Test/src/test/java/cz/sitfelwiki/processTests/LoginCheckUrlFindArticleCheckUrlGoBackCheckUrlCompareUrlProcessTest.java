package cz.sitfelwiki.processTests;

import cz.sitfelwiki.WebDriverSettings;
import org.junit.ComparisonFailure;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;

public class LoginCheckUrlFindArticleCheckUrlGoBackCheckUrlCompareUrlProcessTest extends WebDriverSettings {
    public String urlOfPageWhereOurArticleIsLocated;

    @Test
    public void CompareUrl_CompareUrlOfPageWhereIsOurArticleLocatedWithUrlOfPageWeAreGettingAfterClosingTheArticle_UrlAreSame() {
        login();
        String initialUrlOfThePage = driver.getCurrentUrl();
        while(checkPage == false) {
            try {
                urlOfPageWhereOurArticleIsLocated = driver.getCurrentUrl(); //get url of the page where is located the article that we are looking for
                driver.findElement(By.xpath("//td[1][contains(text(), 'What is Java')]/following-sibling::td/a[contains(@data-original-title, 'View Article')]")).click();
                Thread.sleep(1000); //pause to see it
                checkPage = true;
            } catch (Exception e) {
                waitTillMainPageFullyLoaded();
                driver.findElement(By.xpath("//a[contains(text(), 'Next')]")).click();
            }
        }
        driver.findElement(By.cssSelector("a.btn.btn-primary")).click(); //go back to the page
        String urlOfPageWeAreGettingAfterView = driver.getCurrentUrl(); //get url of the page we are in after getting back from view of the article

        try{
            assertEquals(urlOfPageWhereOurArticleIsLocated, urlOfPageWeAreGettingAfterView); //trying to check if we get to the same page
        } catch (ComparisonFailure TheyAreNotEqual) {
            System.out.println("The function doesn't work properly!");
            assertEquals(urlOfPageWeAreGettingAfterView, initialUrlOfThePage);
        }

    }

}
