package cz.sitfelwiki.processTests;

import cz.sitfelwiki.WebDriverSettings;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class RegisterLoginDeleteAccountLoginAgainProcessTest extends WebDriverSettings {

    /**
    We are testing if combination of registration, authorization, deleting account works correctly
    */

    @Test
    public void RegisterAndLoginWithRandomCredentials_DeleteAccount_TryToLoginAgain() {
        registerUserWithRandomCredentials();
        authorizeUserWithRandomCredentials();
        waitTillMainPageFullyLoaded();
        driver.findElement(By.className("dropdown")).click();
        driver.findElement(By.xpath("//a[contains(text(), 'Delete Your Account')]")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("input.btn.btn-danger")));
        driver.findElement(By.cssSelector("input.btn.btn-danger")).click();
        authorizeUserWithRandomCredentials(); //trying to authorize again with the same credentials
    }
}
