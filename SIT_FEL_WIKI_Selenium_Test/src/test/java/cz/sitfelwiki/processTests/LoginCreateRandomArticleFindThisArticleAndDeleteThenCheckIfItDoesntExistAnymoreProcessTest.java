package cz.sitfelwiki.processTests;

import cz.sitfelwiki.WebDriverSettings;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LoginCreateRandomArticleFindThisArticleAndDeleteThenCheckIfItDoesntExistAnymoreProcessTest extends WebDriverSettings  {

    @Test
    public void Login_CreateArticle_Search_DeleteArticle_Search() {
        login();
        createRandomArticle();
        waitTillMainPageFullyLoaded();
        System.out.println(articleName);


        //Finding the article that has been just created in the main page.
        while (checkPage == false) {
            try {
                driver.findElement(By.xpath("//td[1][contains(text(), '" + articleName + "')]/following-sibling::td/a[contains(@data-original-title, 'Delete Article')]")).click();
                checkPage = true;
            } catch (Exception e) {
                wait.until(ExpectedConditions.presenceOfElementLocated(By.className("pagination")));
                driver.findElement(By.xpath("//a[contains(text(), 'Next')]")).click();
            }
        }

        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("input.btn.btn-danger"))); //wait until warning page is loaded
        driver.findElement(By.cssSelector("input.btn.btn-danger")).click(); //confirm deleting the note
        waitTillMainPageFullyLoaded();

        checkPage = false; //setting checkPage to be false again

        //We want to check all pages again to be sure that the article doesn't exist anymore
        while (checkPage == false) {
            try {
                driver.findElement(By.xpath("//td[1][contains(text(), '" + articleName + "')]/following-sibling::td/a[contains(@data-original-title, 'View Article')]")).click();
                checkPage = true; //exit
            } catch (Exception e) {
                waitTillMainPageFullyLoaded();
                driver.findElement(By.xpath("//a[contains(text(), 'Next')]")).click();

                //we are checking if we are on the last page. if we are and there is no such article it means that we deleted this article.
                try {
                    driver.findElement(By.className("disabled")); //there are only 2 disabled classes. At the first page and at he last.
                    checkPage = true;
                } catch (Exception except) {
                }
            }
        }
    }
}
