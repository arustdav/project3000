package cz.sitfelwiki.processTests;

import cz.sitfelwiki.WebDriverSettings;
import org.junit.Test;
import org.openqa.selenium.By;

public class AuthorizeFindExistingArticleUpdateExistingArticleLogoutProcessTest extends WebDriverSettings {

    @Test
    public void Authorize_FindExistingArticle_UpdateExistingArticle_Logout() {

        login();
        waitTillMainPageFullyLoaded();

        /** we are trying to find existing article in the main page. It has several pages so we are checking all of them until we find our article. */
        while(checkPage == false) {
            try {
                driver.findElement(By.xpath("//td[1][contains(text(),  'Smart pro XS Ultra guide')]/following-sibling::td/a[contains(@data-original-title, 'Update Article')]")).click();
                checkPage = true;
            } catch (Exception e) {
                waitTillMainPageFullyLoaded();  //wait first
                driver.findElement(By.xpath("//a[contains(text(), 'Next')]")).click(); // opening next page
            }
        }

        updateExistingArticle();
        waitTillMainPageFullyLoaded();
        logout();
    }
}
