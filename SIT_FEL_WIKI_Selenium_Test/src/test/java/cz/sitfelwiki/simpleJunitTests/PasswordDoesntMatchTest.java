package cz.sitfelwiki.simpleJunitTests;


import cz.sitfelwiki.WebDriverSettings;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;

public class PasswordDoesntMatchTest extends WebDriverSettings {


    @Test
    public void Register_inputInvalidConfirmPassword_UserDoesntRegisterErrorReturn() {

        //ARRANGE
        String justUsername = "pipkapro";
        String firstFieldPassword = "12345678910";
        String secondFieldPassword = "12345678";
        String error = "Password did not match.";

        //ACT
        driver.get("http://localhost/register.php");
        driver.findElement(By.name("username")).sendKeys(justUsername);
        driver.findElement(By.name("password")).sendKeys(firstFieldPassword);
        driver.findElement(By.name("confirm_password")).sendKeys(secondFieldPassword);
        driver.findElement(By.cssSelector("input[type='submit']")).click();

        //ASSERT
        Assert.assertEquals(error, driver.findElement(By.id("confirmPasswordErrorId")).getText());
    }

}
