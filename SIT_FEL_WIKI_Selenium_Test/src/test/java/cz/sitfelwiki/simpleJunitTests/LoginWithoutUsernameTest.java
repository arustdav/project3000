package cz.sitfelwiki.simpleJunitTests;

import cz.sitfelwiki.WebDriverSettings;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;

public class LoginWithoutUsernameTest extends WebDriverSettings {

    @Test
    public void Login_LeaveUsernameFieldEmpty_UserDoesntAutorizeErrorReturn() {

        //ARRANGE
        String justUsername = "";
        String fieldPassword = "12345678910";
        String error = "Please enter username.";


        //ACT
        driver.get("http://localhost/login.php");
        driver.findElement(By.name("username")).sendKeys(justUsername);
        driver.findElement(By.name("password")).sendKeys(fieldPassword);
        driver.findElement(By.cssSelector("input[type='submit']")).click();

        //ASSERT
        Assert.assertEquals(error, driver.findElement(By.id("usernameErrorId")).getText());
    }
}
