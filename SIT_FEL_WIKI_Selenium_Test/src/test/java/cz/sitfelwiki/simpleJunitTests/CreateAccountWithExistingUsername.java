package cz.sitfelwiki.simpleJunitTests;

import cz.sitfelwiki.WebDriverSettings;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;

public class CreateAccountWithExistingUsername extends WebDriverSettings {

    @Test
    public void Register_InputUsernameThatAlreadyDoesExistInDatabase_UserDoesntRegisterReturnError() {

        //ARRANGE
        String shortUsername = "kvakvakva";
        String correctPassword = "12345678";
        String error = "This username is already taken.";

        //ACT
        driver.get("http://localhost/register.php");
        driver.findElement(By.name("username")).sendKeys(shortUsername);
        driver.findElement(By.name("password")).sendKeys(correctPassword);
        driver.findElement(By.name("confirm_password")).sendKeys(correctPassword);
        driver.findElement(By.cssSelector("input[type='submit']")).click();

        //ASSERT
        Assert.assertEquals(error, driver.findElement(By.id("usernameErrorId")).getText());
    }
}
