package cz.sitfelwiki.simpleJunitTests;

import cz.sitfelwiki.WebDriverSettings;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;

public class LoginWithIncorrectPasswordTest extends WebDriverSettings {

    @Test
    public void Login_InputPasswordThatDoesntExistsInDatabase_UserDoesntAutorizeReturnError() {

        //ARRANGE
        String justUsername = "kvakvakva";
        String fieldPassword = "qwe123rty";
        String error = "The password you entered was not valid.";

        //ACT
        driver.get("http://localhost/login.php");
        driver.findElement(By.name("username")).sendKeys(justUsername);
        driver.findElement(By.name("password")).sendKeys(fieldPassword);
        driver.findElement(By.cssSelector("input[type='submit']")).click();

        //ASSERT
        Assert.assertEquals(error, driver.findElement(By.id("passwordErrorId")).getText());
    }
}
