package cz.sitfelwiki.mockitoTests;

import cz.sitfelwiki.initial.dao.UserDao;
import cz.sitfelwiki.initial.dao.UserDaoImpl;
import cz.sitfelwiki.initial.model.User;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**  Simple JUnit Tests just to check if our methods works properly */

public class UserDaoTest {

    private UserDao dao;

    @Before
    public void setUp() throws Exception{
        this.dao = new UserDaoImpl();
    }


    @Test
    public void getUserByUsername_ShouldReturnTrue_True() throws Exception {
        User valera = dao.getUserByUsername("Valera1414@gmail.com");
        assertThat(valera).isNotNull();
    }

    @Test
    public void getUserByUsername_Null_User() throws Exception {
        User kesha = dao.getUserByUsername("keshka@gmail.com");
        assertThat(kesha).isNull();
    }

    @Test
    public void findAllUsers_Contain() throws Exception {
        List<User> allUsers = dao.findAllUsers();
        assertThat(allUsers).contains(new User("darkmistermaster@gmail.com", "qazxswedc"));
    }
}
