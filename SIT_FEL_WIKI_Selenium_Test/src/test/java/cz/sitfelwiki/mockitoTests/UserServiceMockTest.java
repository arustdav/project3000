package cz.sitfelwiki.mockitoTests;

import cz.sitfelwiki.initial.dao.UserDao;
import cz.sitfelwiki.initial.model.User;
import cz.sitfelwiki.initial.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;

public class UserServiceMockTest {

    /** Individual mock variables */

    @Mock
    private UserDao dao;


    private UserService userService;


    /** Init first */
    public UserServiceMockTest() {
        MockitoAnnotations.initMocks(this);
        this.userService = new UserService(dao);
    }


    /**  Mocking the presence of user */

    @Test
    public void checkUserPresence_ShouldReturnTrue_True() throws Exception{

            /** We define what we want to get from method by ourselves */
            //ARRANGE
            given(dao.getUserByUsername("olga@gmail.com")).willReturn(new User("olga@gmail.com"));

            //ACT
            boolean userExists = userService.checkUserPresence(new User("olga@gmail.com"));

            //ASSERT
            assertThat(userExists).isTrue();
            verify(dao).getUserByUsername("olga@gmail.com");
    }


    /**  Mocking that there is no such user */

    @Test
    public void checkUserPresence_ShouldReturnNull_False() throws Exception{

        /** We define what we want to get from method by ourselves */
        //ARRANGE
        given(dao.getUserByUsername("olga@gmail.com")).willReturn(null);

        //ACT
        boolean userExists = userService.checkUserPresence(new User("olga@gmail.com"));

        //ASSERT
        assertThat(userExists).isFalse();
    }


    /**  Method will throw exception */

    @Test(expected = Exception.class)//ASSERT
    public void checkUserPresence_ShouldThrowException_ThrowException() throws Exception{

        /** We define what we want to get from method by ourselves */
        //ARRANGE
        given(dao.getUserByUsername(anyString())).willThrow(Exception.class);

        //ACT
        userService.checkUserPresence(new User("olga@gmail.com"));
    }


    /** In this way we are able to launch service method and to track what paramenr was used in our dao method*/

    @Test
    public void testCaptor() throws Exception {

        //ARRANGE
        given(dao.getUserByUsername(anyString())).willReturn(new User("olga@gmail.com"));
        boolean b = userService.checkUserPresence(new User("olga@gmail.com"));

        //ACT
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        verify(dao).getUserByUsername(captor.capture());
        String argument = captor.getValue();

        //ASSERT
        assertThat(argument).isEqualTo("olga@gmail.com");
    }

}
