package cz.sitfelwiki.mockitoTests;

import java.sql.Connection;
import java.sql.Statement;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/** Mocking the connection to database */

public class DBConnectionTest {

    /** Individual mock variables */

    @InjectMocks private DBConnection dbConnection;
    @Mock private Connection mockConnection;
    @Mock private Statement mockStatement;


    /** Init first */

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    /** In this test we are trying to mock the connection to DB and check if it works. */

    @Test
    public void ConnectToDB_MockTheConnection_ConnectionIsSuccessful() throws Exception {
        //ARRANGE
        Mockito.when(mockConnection.createStatement()).thenReturn(mockStatement);
        Mockito.when(mockConnection.createStatement().executeUpdate(Mockito.anyString())).thenReturn(1);

        //ACT
        int value = dbConnection.executeQuery("");

        //ASSERT
        Assert.assertEquals(value, 1);
        Mockito.verify(mockConnection.createStatement(), Mockito.times(1));
    }


}


//public class DBConnectionTest {
//
//    @InjectMocks private DBConnection dbConnection;
//    @Mock private Connection mockConnection;
//    @Mock private Statement mockStatement;
//
//    @Mock private User u;
//
//
//    @Before
//    public void setUp() {
//        MockitoAnnotations.initMocks(this);
//
//    }
//
//    @Test
//    public void testMockDBConnection() throws Exception {
//        Mockito.when(mockConnection.createStatement()).thenReturn(mockStatement);
//        Mockito.when(mockConnection.createStatement().executeUpdate(Mockito.anyString())).thenReturn(1);
//        dbConnection.getDBConnection();
//        int value = dbConnection.executeQuery("");
//        Assert.assertEquals(value, 1);
//        Mockito.verify(mockConnection.createStatement(), Mockito.times(1));
//    }
//

//
//
//    @Test
//    public void testNew() {
//       Mockito.when(u.getUsername()).thenReturn("vavakaka");
//       Mockito.when(u.getPassword()).thenReturn("asdasljfsl");
//       Mockito.when(u.getId()).thenReturn(72);
//
//        if(u.getUsername().equals("") == false && u.getUsername().length() >= 8 && u.getId() != 0 && u.getPassword().equals("") == false && u.getPassword().length() >= 8) {
//            try {
//                if(dbConnection != null) {
//                    dbConnection.executeQuery("DELETE FROM users WHERE username = vavakaka");
//                }
//            } catch (ClassNotFoundException e) {
//                e.printStackTrace();
//            } catch (SQLException throwables) {
//                throwables.printStackTrace();
//            }
//            try {
//                Assert.assertNull(DBConnection.executeQuery("SELECT * FROM users WHERE username = 'vavakaka'"));
//            } catch (ClassNotFoundException e) {
//                e.printStackTrace();
//            } catch (SQLException throwables) {
//                throwables.printStackTrace();
//            }
//        }
//    }
//


//}