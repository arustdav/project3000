package cz.sitfelwiki.mockitoTests;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/** Connection to our database */

public class DBConnection {

    private Connection dbConnection;

    public void getDBConnection() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        dbConnection = DriverManager.getConnection("jdbc:mysql://localhost:6666/jcg", "root", "password");
    }

    public int executeQuery(String query) throws ClassNotFoundException, SQLException {
        return dbConnection.createStatement().executeUpdate(query);
    }
}

//public class DBConnection {
//
//    public static Connection dbConnection;
//
//    public void getDBConnection() throws ClassNotFoundException, SQLException {
//        Class.forName("com.mysql.jdbc.Driver");
//        dbConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "root", "newpassword");
//    }
//
//    public  static int executeQuery(String query) throws ClassNotFoundException, SQLException {
//        return dbConnection.createStatement().executeUpdate(query);
//    }

//    public int formQuery(String query) throws ClassNotFoundException, SQLException {
//        return dbConnection.createStatement();
//    }
//


//    public int create(String user) throws ClassNotFoundException, SQLException {
//        return dbConnection.createStatement().executeUpdate(user);
//    }


