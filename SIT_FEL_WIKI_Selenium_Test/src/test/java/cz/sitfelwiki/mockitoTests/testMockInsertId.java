package cz.sitfelwiki.mockitoTests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import java.sql.Connection;
import java.sql.Statement;

/** Again another Mock Test Here */

public class testMockInsertId {

    /** Individual mock variables */

    @InjectMocks
    private DBConnection dbConnection;
    @Mock
    private Connection mockConnection;
    @Mock private Statement mockStatement;


    /** Init first */

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }


    /** In this test we are trying to mock ID and check if after connection we will get it. */

    @Test
    public void FindIdInDB_MockTheID_IdExists() throws Exception {

        //ARRANGE
        Mockito.when(mockConnection.createStatement()).thenReturn(mockStatement);
        Mockito.when(mockConnection.createStatement().executeUpdate(Mockito.anyString())).thenReturn(50);

        //ACT
        int value = dbConnection.executeQuery("INSERT INTO users(id) VALUES (50)");

        //ASSERT
        Assert.assertEquals(value, 50);
        Mockito.verify(mockConnection.createStatement(), Mockito.times(1));
    }
}
