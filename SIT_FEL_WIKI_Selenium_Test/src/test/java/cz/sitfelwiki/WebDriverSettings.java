package cz.sitfelwiki;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Random;

public class WebDriverSettings {
    public Boolean checkPage = false;
    public ChromeDriver driver; //we tast in google chrome
    public WebDriverWait wait; //pause between operations
    Random rand = new Random();

    /**
    Random String generators for Password Username and Articles.
    Static in case we will need to use the same combination again in other tests
    */
    public static String correctPassword = RandomStringUtils.randomAlphabetic(10);
    public static String shortUsername = RandomStringUtils.randomAlphabetic(10);
    public static String articleName = RandomStringUtils.randomAlphabetic(10);



    /** we setup driver before each test */
    @Before
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "/Users/cloudlight/Documents/TS1/chromedriver");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10, 500);
    }

    /** we close everything after tests */
    @After
    public void close() throws InterruptedException {
        if(driver != null) {
            Thread.sleep(1999);
            driver.quit();
        }

    }

    /** authorize with existing credentials */
    public void login() {
        driver.get("http://localhost/login.php"); //get page
        driver.findElement(By.name("username")).sendKeys("speliyapelsin"); //find fields and input data
        driver.findElement(By.name("password")).sendKeys("KrasnayaMorkov"); //find fields and input data
        driver.findElement(By.cssSelector("input[type='submit']")).click(); //find button and click it
    }

    /** register with random credentials */
    public void registerUserWithRandomCredentials() {
        driver.get("http://localhost/register.php");
        System.out.println("username is " + shortUsername); //print username in case we will need it
        System.out.println("password is " + correctPassword); //print password in case we will need it


        driver.findElement(By.name("username")).sendKeys(shortUsername);
        driver.findElement(By.name("password")).sendKeys(correctPassword);
        driver.findElement(By.name("confirm_password")).sendKeys(correctPassword);
        driver.findElement(By.cssSelector("input[type='submit']")).click();

    }

    /** authorize user with created combination of username and password */
    public void authorizeUserWithRandomCredentials() {
        driver.get("http://localhost/login.php");
        driver.findElement(By.name("username")).sendKeys(shortUsername);
        driver.findElement(By.name("password")).sendKeys(correctPassword);
        driver.findElement(By.cssSelector("input[type='submit']")).click();
    }

    /**
     create random article with 10 symbols in article name, 3or8 symbols in subject and 256 in article body
    */
    public void createRandomArticle() {
        driver.findElement(By.cssSelector("a.btn.btn-success.pull-right")).click();

        driver.findElement(By.name("article_name")).sendKeys(articleName);
        if (rand.nextInt()%2 == 0){ //we want 3 or 8 symbols in "subject" field
            driver.findElement(By.name("subject")).sendKeys(RandomStringUtils.randomAlphabetic(3));
        } else {
            driver.findElement(By.name("subject")).sendKeys(RandomStringUtils.randomAlphabetic(8));
        }
        driver.findElement(By.name("article_body")).sendKeys(RandomStringUtils.randomAlphabetic(256));
        driver.executeScript("window.scrollBy(0,1000)"); //we scroll the window  down in order to find the button
        driver.findElement(By.cssSelector("input[type='submit']")).click();
    }

    public void updateExistingArticle() {
        driver.findElement(By.name("article_body")).clear(); //clear the article body
        driver.findElement(By.name("article_body")).sendKeys(RandomStringUtils.randomAlphabetic(256)); //random input
        driver.executeScript("window.scrollBy(0,1000)"); //scroll the window in to find the submit button
        driver.findElement(By.cssSelector("input[type='submit']")).click();
    }

    /** logout if we are on the main page */
    public void logout() {
        driver.findElement(By.className("dropdown")).click();
        driver.findElement(By.xpath("//a[contains(text(), 'Sign Out of Your Account')]")).click();
    }

    /** we need to wait  to prevent accidental click in nowhere etc. . . */
    public void waitTillMainPageFullyLoaded() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.className("pagination")));
    }
}
