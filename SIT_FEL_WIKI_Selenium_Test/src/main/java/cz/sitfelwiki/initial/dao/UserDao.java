package cz.sitfelwiki.initial.dao;

import cz.sitfelwiki.initial.model.User;

import java.util.List;

/** User Data Access Object interface where all general methods are described */

public interface UserDao {

   /** This method returns User via his username */
   User getUserByUsername(String username) throws Exception;

   /** This method finds all users */
   List<User> findAllUsers();

}
