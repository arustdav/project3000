package cz.sitfelwiki.initial.dao;

import cz.sitfelwiki.initial.model.User;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/** Implementation of UserDao interface */

public class UserDaoImpl implements UserDao {

    /** List with all users */

    private List<User> users;

    /** Constructor for our class. We are creating list with users */

    public UserDaoImpl() {
        this.users = Arrays.asList(

                new User("Lariska228@mail.ru", "qwertyui"),
                new User("darkmistermaster@gmail.com", "qazxswedc"),
                new User("Valera1414@gmail.com", "chytreheslo")

        );
    }

    /** Implementation of Interface method */

    public User getUserByUsername(String username) throws Exception {
        return  users.stream().filter(u->u.getUsername().equals(username)).findAny().orElse(null);
    }


    /** Implementation of Interface method */

    @Override
    public List<User> findAllUsers() {
         return users;
    }
}
