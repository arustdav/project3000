package cz.sitfelwiki.initial.services;

import cz.sitfelwiki.initial.dao.UserDao;
import cz.sitfelwiki.initial.model.User;

/** User service class. Contains only 1 method to check presence of user */

public class UserService {

    private UserDao dao;


    public UserService(UserDao dao) {
        this.dao = dao;
    }


    /** Check if user exists */

    public boolean checkUserPresence(User user) throws Exception{
        User u = dao.getUserByUsername(user.getUsername());
        return u != null;
    }
}
