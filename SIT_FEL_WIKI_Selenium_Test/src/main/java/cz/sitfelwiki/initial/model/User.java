package cz.sitfelwiki.initial.model;

import java.util.Objects;

/** Our user. "Table" from our "Database". */

public class User {

    /** User id. As in DB */
    public int id;

    /** User username. As in DB */
    public String username;

    /** User password. As in DB */
    public String password;


    /** Constructor. We initialise only username and password */
    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    /** Constructor. We initialise only username */
    public User(String username) {
        this.username = username;
    }

   /** Individual Getters and Setters below */

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                Objects.equals(username, user.username) &&
                Objects.equals(password, user.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, password);
    }

}
