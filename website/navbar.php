<nav class="navbar navbar-inverse navbar-top">
    <!-- Navbar Container -->
        <div class="container">
            <!-- Navbar Header [contains both toggle button and navbar brand] -->
            <div class="navbar-header">
                <!-- Toggle Button [handles opening navbar components on mobile screens]-->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#exampleNavComponents" aria-expanded="false">
                    <i class="glyphicon glyphicon-align-center"></i>
                </button>
                <!-- Navbar Brand -->
                <a href="#" class="navbar-brand">
                    SIT FEL Wiki
                </a>
            </div>
            <!-- Navbar Collapse [contains navbar components such as navbar menu and forms ] -->
            <div class="collapse navbar-collapse" id="exampleNavComponents">
                <!-- Navbar Menu -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Navbar link with a dropdown menu -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Account Actions
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="reset-password.php">Reset Your Password</a></li>
                            <li><a href="logout.php">Sign Out of Your Account</a></li>
                            <li><a href="deleteUser.php?username='<?php echo htmlspecialchars($_SESSION["username"]);?>'">Delete Your Account</a></li>
                        </ul>
                    </li>
                </ul>
                <p class="navbar-text">Hello, <b><?php 
                                                    if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
                                                        echo htmlspecialchars($_SESSION["username"]);
                                                    }else{
                                                        echo "stranger";
                                                    } ?></b></p>   
            </div>
        </div>
</nav>