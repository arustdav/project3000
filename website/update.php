<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "config.php";
 
// Define variables and initialize with empty values
$article_name = $subject = $article_body = "";
$article_name_err = $article_name_err_limit = $subject_err = $subject_err_limit = $article_body_err = $article_body_err_limit = "";
 
// Processing form data when form is submitted
if(isset($_POST["id"]) && !empty($_POST["id"])){
    // Get hidden input value
    $id = $_POST["id"];

    // Validate article_name
    $input_article_name = trim($_POST["article_name"]);
    if(empty($input_article_name)){
        $article_name_err = "Please enter an article name.";
    } elseif(strlen(trim($_POST["article_name"])) > 32){
        $article_name_err_limit = "Article name can be maximum 32 characters long.";
    } else{
        $article_name = $input_article_name;
    }

    // Validate subject
    $input_subject = trim($_POST["subject"]);
    if(empty($input_subject)){
        $subject_err = "Please enter a subject.";
    } elseif(strlen(trim($_POST["subject"])) != 8 && strlen(trim($_POST["subject"])) != 3){
        $subject_err_limit = "Subject must be only 3 or 8 characters long.";
    } else{
        $subject = $input_subject;
    }

    // Validate article_body
    $input_article_body = trim($_POST["article_body"]);
    if(empty($input_article_body)){
        $article_body_err = "Please enter an article body.";
    } elseif(strlen(trim($_POST["article_body"])) > 1024){
        $article_body_err_limit = "Article name can be maximum 1024 characters long.";
    } else{
        $article_body = $input_article_body;
    }
    
    // Check input errors before inserting in database
    if(empty($article_name_err) && empty($article_name_err_limit) && empty($subject_err) && empty($subject_err_limit) && empty($article_body_err) && empty($article_body_err_limit)){
        // Prepare an update statement
        $sql = "UPDATE articles SET article_name=:article_name, subject=:subject, article_body=:article_body WHERE id=:id";
 
        if($stmt = $pdo->prepare($sql)){
            // Bind variables to the prepared statement as parameters
            $stmt->bindParam(":article_name", $param_article_name);
            $stmt->bindParam(":subject", $param_subject);
            $stmt->bindParam(":article_body", $param_article_body);
            $stmt->bindParam(":id", $param_id);
            
            // Set parameters
            $param_article_name = $article_name;
            $param_subject = $subject;
            $param_article_body = $article_body;
            $param_id = $id;
            
            // Attempt to execute the prepared statement
            if($stmt->execute()){
                // Articles updated successfully. Redirect to landing page
                header("location: index.php");
                exit();
            } else{
                echo "Something went wrong. Please try again later.";
            }
        }
         
        // Close statement
        unset($stmt);
    }
    
    // Close connection
    unset($pdo);
} else{
    // Check existence of id parameter before processing further
    if(isset($_GET["id"]) && !empty(trim($_GET["id"]))){
        // Get URL parameter
        $id =  trim($_GET["id"]);
        
        // Prepare a select statement
        $sql = "SELECT * FROM articles WHERE id = :id";
        if($stmt = $pdo->prepare($sql)){
            // Bind variables to the prepared statement as parameters
            $stmt->bindParam(":id", $param_id);
            
            // Set parameters
            $param_id = $id;
            
            // Attempt to execute the prepared statement
            if($stmt->execute()){
                if($stmt->rowCount() == 1){
                    /* Fetch result row as an associative array. Since the result set
                    contains only one row, we don't need to use while loop */
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);
                
                    // Retrieve individual field value
                    $article_name = $row["article_name"];
                    $subject = $row["subject"];
                    $article_body = $row["article_body"];
                } else{
                    // URL doesn't contain valid id. Redirect to error page
                    header("location: error.php");
                    exit();
                }
                
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
        
        // Close statement
        unset($stmt);
        
        // Close connection
        unset($pdo);
    }  else{
        // URL doesn't contain id parameter. Redirect to error page
        header("location: error.php");
        exit();
    }
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>SIT FEL Wiki - Update Article</title>
    <link rel="shortcun icon" href="images/logo-sfw.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>
    <?php
        require_once('navbar.php');
    ?>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h2>Update Article</h2>
                    </div>
                    <p>Please edit the input values and submit to update the article.</p>
                    <form action="<?php echo htmlspecialchars(basename($_SERVER['REQUEST_URI'])); ?>" method="post">
                        <div class="form-group <?php echo (!empty($article_name_err) || !empty($article_name_err_limit)) ? 'has-error' : ''; ?>">
                            <label>Article Name</label>
                            <input type="text" name="article_name" class="form-control" value="<?php echo htmlspecialchars($article_name); ?>">
                            <span class="help-block"><?php echo $article_name_err; echo $article_name_err_limit?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($subject_err) || !empty($subject_err_limit)) ? 'has-error' : ''; ?>">
                            <label>Subject</label>
                            <input type="text" name="subject" class="form-control" value="<?php echo htmlspecialchars($subject); ?>">
                            <span class="help-block"><?php echo $subject_err; echo $subject_err_limit?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($article_body_err) || !empty($article_body_err_limit)) ? 'has-error' : ''; ?>">
                            <label>Article Body</label>
                            <textarea name="article_body" class="form-control" rows="20" cols="50"><?php echo htmlspecialchars($article_body); ?></textarea>
                            <span class="help-block"><?php echo $article_body_err; echo $article_body_err_limit?></span>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                        <input type="submit" class="btn btn-primary" value="Submit">
                        <a href="index.php" class="btn btn-default">Cancel</a>
                    </form>
                </div>
            </div>        
        </div>
    </div>
    <?php
        require_once('footer.php');
    ?>
</body>
</html>